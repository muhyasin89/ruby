require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
    test "login with Invalid information" do
        get login_path
        assert_template 'auth/login'
        post login_path, params: { :email => session[:email], :password => session[:password]  }
        assert_template 'auth/login'
        assert_not flash.empty?
        get root_path
        assert flash.empty?
    end
end
