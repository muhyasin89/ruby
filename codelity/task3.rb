# you can write to stdout for debugging purposes, e.g.
# puts "this is a debug message"

def check_dif(a,b)
    if(a == b)
         return 1
    else
         return 0
    end
end

def solution(a)
  # write your code in Ruby 2.2
  @sum=0
  cur_index=0
  n = a.length
  lastnum =n -1

  i=0
  while i<n do
      $res=check_dif(a[i],a[i+1])
      #jika nilai sama
      if($res)
         @sum +=1
          cur = a[i+2]
          cur_index +=2
      else
          temp = a[i]
          cur = a[i+1]
          cur_index +=1
      end

      k_index=cur_index+1
      for k in k_index..lastnum
          $res_k= check_dif(temp,a[k])
          if(!$res_k)
              $res_k1= check_dif(cur,a[k])
              if($res_k)
                  @sum +=1
                  $res_k2 = check_dif(temp,a[k+1])
              else
                  next
              end
          else
              @sum += 1
          end
      end
      #end of while
    end

  return @sum
end


nilai =[5,4,4,5,0,12]
# nilai =Hash[nilai.map.with_index.to_a]
 result = solution(nilai)

 puts result
#puts nilai.inspect
