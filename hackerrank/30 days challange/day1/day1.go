package main


import "fmt"


func add_int(i int,j int)int{
    return i+j
}

func add_float(d float,e float)float{
    return d+e
}

func add_string(s string,h string){
    return s+h
}

func main(){

    i=4
    d=4.0
    s='HackerRank '

    // Declare second integer, double, and String variables.
    j=12
    e = 4.0
    h= 'is the best place to learn and practice coding!'
    // Read and save an integer, double, and String to your variables.

        k=add_int(i,j)
        f=add_float(d,e)
        l=add_string(s,h)

        // Print the sum of both integer variables on a new line.
        fmt.Println(k)
        // Print the sum of the double variables on a new line.
        fmt.Println(f)

        // Concatenate and print the String variables on a new line
        // The 's' variable above should be printed first.
        fmt.Println(l)

}
