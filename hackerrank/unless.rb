def scoring(array)

  # update_score of every user in the array unless the user is admin
    array.each {|user|
          user.update_score unless is_admin?
        }
end

#option 2
# def scoring(array)
#     unless user.is_admin?
#             user.update_score
#         end
# end

array = Array[10,1,2,3,2,1,1,2,3,4,1]

scoring(array)
